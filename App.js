import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Constants from "expo-constants";
import ListScreen from "./src/screens/ListScreen";

import useGlobalState from "./src/store/useGlobalState";
import Context from "./src/store/context";

export default function App() {
  const store = useGlobalState();
  return (
    <Context.Provider value={store}>
      <SafeAreaView style={styles.container}>
        <ListScreen />
      </SafeAreaView>
    </Context.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: "#fff"
  }
});

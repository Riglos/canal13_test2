# Juego de preguntas y respuestas.

## Build Setup

```bash
# install dependencies
expo install

# serve with hot reload at localhost:8080
expo start

# build APK for production
expo build:android
```

## Description

Es una aplicación que muestra de a 1 pregunta al azar junto con su respuestas, el usuario presiona sobre la respuesta que crea correcta y al finalizar las 10 preguntas, se puede volver a comenzar, haciendo un llamado a una API para traerse 10 preguntas al azar.

## Notes

- `react-native-elements` para la interfaz de usuario, `AXIOS` para la llamada a la API, `react-native-vector-icons` para utilizar los iconos, en este caso particular el header de la pregunta, `react-native-touchable-scale` al presionar sobre la respuesta, y `expo-constants` para manejar el tamaño del statusheader.

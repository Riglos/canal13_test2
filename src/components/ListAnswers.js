import React, { useContext } from "react";
import TouchableScale from "react-native-touchable-scale";
import { ListItem } from "react-native-elements";
import Context from "../store//context";
import { pressOption } from "../services/handler";

export default function ListAnswers() {
  const { state, actions } = useContext(Context);
  return state.data[state.randomNumber].answers.map((item, i) => (
    <ListItem
      key={i}
      component={TouchableScale}
      onPress={() => pressOption(item, i, actions, state)}
      friction={90}
      tension={100}
      activeScale={0.95}
      linearGradientProps={{
        colors: ["#4CAF50", "#8BC34A"],
        start: [1, 0],
        end: [0.2, 0]
      }}
      title={item.answer}
      titleStyle={{ color: "white", fontWeight: "bold" }}
      chevronColor="white"
      chevron
      containerStyle={{
        marginHorizontal: 16,
        marginVertical: 8,
        borderRadius: 8
      }}
    />
  ));
}

import React, { useContext } from "react";
import Context from "../store/context";
import { View, StyleSheet, Text, Dimensions } from "react-native";
import { Overlay, Button, Divider } from "react-native-elements";
import { functionmakeRemoteRequest } from "../services/api";
const windowWidth = Dimensions.get("window").width;

export default function OverlayComponent() {
  const { state, actions } = useContext(Context);
  return (
    <Overlay isVisible={state.finish}>
      <View style={styles.container}>
        <Text style={styles.title}>Muchas gracias por jugar!</Text>
        <View style={styles.content}>
          <Text style={styles.corrects}>Correctas: {state.corrects}</Text>
          <Text style={styles.wrongs}>Incorrectas: {state.wrongs}</Text>
          <Divider style={styles.line} />
          <View style={styles.containerTotal}>
            <Text style={styles.total}>
              Total: {state.wrongs + state.corrects}
            </Text>
          </View>
        </View>
        <View style={styles.footer}>
          <Button
            title="Play Again"
            onPress={() => {
              functionmakeRemoteRequest({ state, actions });
            }}
          />
        </View>
      </View>
    </Overlay>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-around",
    alignContent: "center"
  },
  title: {
    alignSelf: "center",
    justifyContent: "center",
    fontSize: 18
  },
  content: {
    flex: 0.4,
    alignItems: "center",
    justifyContent: "space-around"
  },
  corrects: {
    marginHorizontal: 16,
    color: "green",
    fontSize: 16,
    fontWeight: "bold"
  },
  total: {
    marginHorizontal: 16,
    color: "black",
    fontSize: 16,
    fontWeight: "bold"
  },
  containerTotal: {
    borderWidth: 1
  },
  wrongs: {
    marginHorizontal: 16,
    color: "red",
    fontSize: 16,
    fontWeight: "bold"
  },
  line: {
    borderWidth: 1,
    width: windowWidth / 3,
    marginTop: 30
  },
  footer: { marginHorizontal: 30 }
});

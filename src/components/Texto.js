import React from "react";
import { StyleSheet } from "react-native";
import { Text } from "react-native-elements";

export default function Texto({ data, corrects, wrongs, total, heading }) {
  const textStyles = [
    corrects && styles.corrects,
    wrongs && styles.wrongs,
    total && styles.total,
    heading && styles.heading
  ];

  return <Text style={textStyles}>{data != undefined ? data : "Loading"}</Text>;
}

const styles = StyleSheet.create({
  heading: {
    color: "white",
    marginTop: 10,
    fontSize: 22,
    fontWeight: "bold"
  },
  corrects: { marginHorizontal: 16, color: "green" },
  wrongs: { marginHorizontal: 16, color: "red" },
  total: { marginHorizontal: 16, color: "black" }
});

import React, { useEffect, useContext } from "react";
import Context from "../store/context";

import Loading from "../components/Loading";
import Icon from "react-native-vector-icons/FontAwesome";
import ListAnswers from "../components/ListAnswers";
import Texto from "../components/Texto";
import OverlayComponent from "../components/OverlayComponent";
import { functionmakeRemoteRequest } from "../services/api";
import { checkGame } from "../services/handler";
import { View, StyleSheet } from "react-native";

export default function ListScreen() {
  const { state, actions } = useContext(Context);
  useEffect(() => {
    functionmakeRemoteRequest({ state, actions });
  }, []);

  useEffect(() => {
    checkGame(state, actions);
  }, [state.data]);

  return (
    <View style={styles.container}>
      {state.finish && <OverlayComponent />}
      {!state.finish && state.data.length != 0 ? (
        <View>
          <View style={[styles.headerContainer]}>
            <Icon color="white" name="magic" size={32} type="font-awesome" />
            <Texto data={state.data[state.randomNumber].question} heading />
          </View>
          <View style={styles.content}>
            <ListAnswers />
            <View style={styles.footer}>
              <Texto data={"Corrects: " + state.corrects} corrects />
              <Texto data={"Total: " + (state.wrongs + state.corrects)} total />
              <Texto data={"Wrongs: " + state.wrongs} wrongs />
            </View>
          </View>
        </View>
      ) : (
        <Loading />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
    backgroundColor: "#4F80E1"
  },
  content: {
    paddingVertical: 16
  },
  footer: {
    paddingVertical: 16,
    flexDirection: "row",
    alignContent: "space-between",
    justifyContent: "space-between"
  }
});

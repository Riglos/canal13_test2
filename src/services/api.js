import axios from "axios";
import { URL } from "../constants/theme";

export const functionmakeRemoteRequest = ({ state, actions }) => {
  // const url = `http://5e16456b22b5c600140cf9bf.mockapi.io/api/v1/test?limit=10&page=${state.page}`;
  const url = `${URL} + ${state.page}`;
  console.log(url);
  return axios
    .get(url)
    .then(res => {
      const max = 10;
      const min = 1;
      actions({
        type: "setState",
        payload: {
          ...state,
          data: res.data,
          corrects: 0,
          wrongs: 0,
          page: Math.floor(Math.random() * (max - min)) + min,
          finish: false
        }
      });
    })
    .catch(err => {
      actions({
        type: "setState",
        payload: {
          ...state,
          error: err.message
        }
      });
    });
};

export const checkGame = (state, actions) => {
  if (state.corrects + state.wrongs == 10) {
    actions({
      type: "setState",
      payload: {
        ...state,
        finish: true
      }
    });
  }
};

export const pressOption = (item, i, actions, state) => {
  // ELIMINA RESPUESTA INCORRECTA
  let newArr = [...state.data]; // copying the old datas array
  newArr[state.randomNumber].answers.splice(i, 1); // replace e.target.value with whatever you want to change it to
  correcta(item, actions, state, newArr);
};

correcta = (item, actions, state, newArr) => {
  // let newArr = [...state.data]; // copying the old datas array
  let newArray = [...newArr];
  newArray.splice(state.randomNumber, 1); // replace e.target.value with whatever you want to change it to
  const min = 0;
  //TOMAR MAXIMO DEL NEWARRAY PORQUE DATA TODAVIA NO SE ACTUALIZO
  let max = newArray.length;
  actions({
    type: "setState",
    payload: {
      ...state,
      corrects: item.isCorrect ? state.corrects + 1 : state.corrects,
      wrongs: item.isCorrect ? state.wrongs : state.wrongs + 1,
      data: newArray,
      randomNumber: Math.floor(Math.random() * (max - min)) + min
    }
  });
};

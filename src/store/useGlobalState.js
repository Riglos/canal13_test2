import { useState } from "react";

const useGlobalState = () => {
  const [state, setState] = useState({
    data: [],
    corrects: 0,
    wrongs: 0,
    page: 1,
    randomNumber: 1,
    error: null,
    finish: false
  });

  const actions = action => {
    const { type, payload } = action;
    switch (type) {
      case "setState":
        return setState(payload);
      default:
        return state;
    }
  };
  return { state, actions };
};

export default useGlobalState;
